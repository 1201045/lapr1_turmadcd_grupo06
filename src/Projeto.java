import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;
import java.util.Scanner;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.decomposition.EigenDecompositor;
import java.awt.*;
import java.util.concurrent.TimeUnit;


public class Projeto {

 /*  Indice:
     Leitura comando: ->
        Modo Não Interativo  ->
        Modo Interativo      ->
     Leitura Dados ->
     Distribuição  ->
     Gráficos      ->

     Módulos        ->
         Distribuição etc.     ->
         Letira de dados       ->
         Valor e Vetor Proprio ->
         Gráficos              ->

     */



//Variaveis globais
    static Scanner input = new Scanner(System.in);
    static int tamanhoSemFicheiro = 0;


    public static void main(String[] args) throws IOException, InterruptedException {
        input.useLocale(Locale.ENGLISH);        //Para ler "." nos valores não inteiros
        /*Como executar:
         * Adicionar um artifact: File-> Project Structure -> Artifacts -> "+" -> JAR -> From modules with... -> Main Class: Nome_Classe ->
         * Converter para .jar: Build -> Build Artifacts -> Build/Rebuild
         * Executar o ficheiro: (Diretorio correto no cmd) -> java -jar (argumentos)*/


//Leitura do comando no cmd

        String nomeFicheiroEntrada = "", nomeFicheiroSaida = "";
        boolean ficheiro;
        int numGeracoes, formato = 0;
        String modo;
        boolean VVproprio = false, dimensao_pop = false, variacao_pop = false;

        if (args.length != 0) {
            if (args.length > 9) {
                System.out.println("Comando incorreto. Tente novamente.");
                return;
            }


//====================================================================================================================== Modo não interativo


            if (args[0].equals("-t") || args[0].equals("-g") || args[0].equals("-e") || args[0].equals("-v") || args[0].equals("-r")) {
                int i;
                numGeracoes = -2;
                formato = -1;
                modo = "Nao Interativo";
                for (i = 0; i < args.length - 2; i++) {

                    //Vailidações
                    if (args[i].equals("-g")) {
                        //Formato
                        if (!(args[i + 1].equals("1") || args[i + 1].equals("2") || args[i + 1].equals("3"))) {
                            System.out.println("Comando inválido!Por favor utilize um comando válido.Formato inválido.");
                            System.out.println("1=png,2=txt,3=eps");
                            return;
                        }
                        formato = Integer.parseInt(args[i + 1]);
                    }
                    if (args[i].equals("-t")) {
                        numGeracoes = Integer.parseInt(args[i + 1]);
                        if (numGeracoes<0){
                            System.out.println("Numero de geracoes invalido.");
                            return;
                        }
                    }
                    if (args[i].equals("-e")) {
                        VVproprio = true;                 //Valor e Vetor próprio
                    }
                    if (args[i].equals("-v")) {        //Dimensão total
                        dimensao_pop = true;
                    }
                    if (args[i].equals("-r")) {      //Variação da população
                        variacao_pop = true;
                    }

                }

                if (numGeracoes == -2) {
                    System.out.println("Comando incorreto. O número de geraçoes não foi introduzido.");
                    return;
                }
                if (formato == -1) {
                    System.out.println("Comando incorreto. O formato não foi introduzido.");
                    return;
                }
                nomeFicheiroEntrada = args[i];
                nomeFicheiroSaida = args[i + 1];

                if ((nomeFicheiroEntrada.endsWith(".txt") && (nomeFicheiroSaida.endsWith(".txt")))) {
                    ficheiro = true;
                } else {
                    System.out.println("Não colocou o(s) ficheiro(s) de entrada/saida corretamente!");
                    return;
                }

            }


//====================================================================================================================== Modo interativo


            else {


                modo = "Interativo";
                if (args[0].equals("-n")) {
                    nomeFicheiroEntrada = args[1];  //Pandas.txt
                    if (!nomeFicheiroEntrada.endsWith(".txt")){
                        System.out.println("Formato do ficheiro incorreto. Expected: .txt");
                        return;
                    }
                    if (!new File(nomeFicheiroEntrada).exists()){
                        System.out.println("Erro: Ficheiro não foi encontrado.");
                        return;
                    }
                    //Verificar se existe e acaba em txt

                    nomeFicheiroSaida = args[1].substring(0, args[1].length() - 4);
                    numGeracoes = -1;
                    ficheiro = true;
                    //Bom dia/tarde/noite

                    GregorianCalendar time = new GregorianCalendar();
                    int hour = time.get(Calendar.HOUR_OF_DAY);

                    if (hour < 12)
                        System.out.println("Bom dia.");
                    else if (hour < 17)
                        System.out.println("Boa tarde.");
                    else
                        System.out.println("Boa noite.");
                    System.out.println("Este programa foi desenvolvido com o intuito de prever a evolução de uma determinada espécie." +
                            "\nAo longo do decorrer da aplicação, escolhas devem ser feitas. Em certas perguntas será pedido para responder \"(1.Sim/2.Nao)\", pelo que deverá responder o numero associado a sua resposta.\n");
                } else {
                    System.out.println("Comando incorreto.");
                    return;
                }

            }
        } else {

            //Bom dia/tarde/noite

            GregorianCalendar time = new GregorianCalendar();
            int hour = time.get(Calendar.HOUR_OF_DAY);
            if (hour < 12)
                System.out.println("Bom dia.");
            else if (hour < 17)
                System.out.println("Boa tarde.");
            else
                System.out.println("Boa noite.");
            System.out.println("Este programa foi desenvolvido com o intuito de prever a evolução de uma determinada espécie." +
                    "\nAo longo do decorrer da aplicação, escolhas devem ser feitas. Em certas perguntas será pedido para responder \"(1.Sim/2.Nao)\", pelo que deverá responder o numero associado a sua resposta.\n");

            modo = "Interativo";
            numGeracoes = -1;
            ficheiro = false;
        }
        // System.out.println("VVproprio= " + VVproprio + " dimensao_pop= " + dimensao_pop + " variacao_pop= " + variacao_pop);



//PROGRAMA EM SI

        String repetir = "2";
        do {                      //Para o programa repetir


// Criar Pastas para os ficheiros intermédios e finais

            try {
                Path path1 = Paths.get("./Intermediate_Files");      //Caminho para a pasta
                Path path2 = Paths.get("./Final_Files");      //Caminho para a pasta
                Files.createDirectories(path1);
                Files.createDirectories(path2);
            } catch (IOException e) {
                System.err.println("Failed to create directory!" + e.getMessage());
            }


//====================================================================================================================== LEITURA DE DADOS


            double[][] vetor;
            double[] taxaSi, taxaFi;  // Estas 3 linhas são comuns ao input com e sem ficheiro
            double[][] matrizLeslie;


// INPUT COM FICHEIRO

            if (ficheiro) {

                int tamanho = calcularNumValores(nomeFicheiroEntrada);
                double[][] vetorFicheiro = new double[tamanho][1];
                double[] taxaSiFicheiro = new double[tamanho - 1], taxaFiFicheiro = new double[tamanho];  // Estas 3 linhas são comuns ao input com e sem ficheiro

                while (!lerFicheiro(vetorFicheiro, taxaSiFicheiro, taxaFiFicheiro, nomeFicheiroEntrada)) {
                    if (modo.equals("Interativo")) {
                        System.out.println("Insira novamente um ficheiro");
                        nomeFicheiroEntrada = input.nextLine();
                    } else return;
                }
                vetor = vetorFicheiro;
                taxaSi = taxaSiFicheiro;
                taxaFi = taxaFiFicheiro;
            } else {


// INPUT MANUAL

                double[][] vetorEnorme = new double[200][1];
                double[] taxaSiEnorme = new double[199];
                double[] taxaFiEnorme = new double[200];
                if (lerVetoresEnormes(vetorEnorme, taxaSiEnorme, taxaFiEnorme)) {
                    vetor = passarVetorEnormeNormal(vetorEnorme);
                    taxaSi = passarTaxaSiEnormeNormal(taxaSiEnorme);
                    taxaFi = passarTaxaFiEnormeNormal(taxaFiEnorme);
                } else {


// INPUT SEMI MANUAL

                    System.out.println("Insira o nome do ficheiro");
                    nomeFicheiroEntrada = input.next();
                    ficheiro = true;

                    while (!nomeFicheiroEntrada.endsWith(".txt")) {
                        System.out.println("Nome do ficheiro inválido. (Fomato: .txt)");
                        nomeFicheiroEntrada = input.next();
                    }
                    nomeFicheiroSaida = nomeFicheiroEntrada.substring(0, nomeFicheiroEntrada.length() - 4);
                    int tamanho = calcularNumValores(nomeFicheiroEntrada);
                    double[][] vetorFicheiro = new double[tamanho][1];
                    double[] taxaSiFicheiro = new double[tamanho - 1], taxaFiFicheiro = new double[tamanho];  // Estas 3 linhas são comuns ao input com e sem ficheiro

                    while (!lerFicheiro(vetorFicheiro, taxaSiFicheiro, taxaFiFicheiro, nomeFicheiroEntrada)) {
                        System.out.println("Insira novamente um ficheiro");
                        nomeFicheiroEntrada = input.next();
                    }
                    vetor = vetorFicheiro;
                    taxaSi = taxaSiFicheiro;
                    taxaFi = taxaFiFicheiro;
                }
            }


//Criar diretório final (Onde ficam guardados os ficheiros finais)

            String dir = "";
            if (modo.equals("Interativo") && !(ficheiro)) {
                input.nextLine();
                System.out.println("Introduza o nome da especie");
                nomeFicheiroSaida = input.nextLine();
            }
            try {
                //Caminho para a pasta
                Path path1;
                Path path2;
                if (modo.equals("Interativo")) {
                    dir = "./Final_Files/" + nomeFicheiroSaida;
                } else {
                    dir = "./Final_Files/" + nomeFicheiroEntrada.substring(0, nomeFicheiroEntrada.length() - 4);
                }
                path1 = Paths.get(dir);
                path2 = Paths.get(dir + "/Graficos");
                Files.createDirectories(path1);
                Files.createDirectories(path2);
            } catch (IOException e) {
                System.err.println("Failed to create directory!" + e.getMessage());
            }


//Nome do ficheiro de saida

            LocalDate date = LocalDate.now();                                           //data do momento
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            String data = date.format(formatter);
            String nome;
            if (modo.equals("Interativo")) {
                nome = nomeFicheiroSaida + "." + data;
            } else {
                nome = nomeFicheiroSaida;
            }
            File Ficheiro_Saida = new File(dir + "/" + nome + ".txt");   //Cria ficheiro dentro da pasta
            PrintWriter printWriter5 = new PrintWriter(Ficheiro_Saida);
            



//Construir a Matriz Leslie

            matrizLeslie = atribuirMatrizLeslie(taxaSi, taxaFi);
//        mostrarMatriz(matrizLeslie)
//        mostrarMatriz(vetor);


//Ler numero de geraçoes (Modo interativo)

            if (modo.equals("Interativo")) {
                System.out.println("Introduza as gerações que pretende calcular");
                numGeracoes = input.nextInt();
                while (numGeracoes < 0) {
                    System.out.println("Valor inválido.Insira outro valor");
                    numGeracoes = input.nextInt();
                }
            }


//Escrever a Matriz Leslie e o numero de gerações no ficheiro final

            printWriter5.println("k=" + numGeracoes);
            printWriter5.println("Matriz Leslie");
            printWriter5.println();
            for (int linha = 0; linha < matrizLeslie.length; linha++) {
                for (int coluna = 0; coluna < matrizLeslie[0].length; coluna++) {
                    printWriter5.print(round(matrizLeslie[linha][coluna]) + " ");        //Arredondamento duas casas decimais
                }
                printWriter5.println();
            }


//Criar Ficheiros temporários

            File Populacao_Total = new File("./Intermediate_Files/Populacao_total.txt");
            PrintWriter printWriter = new PrintWriter(Populacao_Total);

            File file2 = new File("./Intermediate_Files/Variacao.txt");
            PrintWriter printWriter2 = new PrintWriter(file2);

            File file3 = new File("./Intermediate_Files/Classes.txt");
            PrintWriter printWriter3 = new PrintWriter(file3);

            File file4 = new File("./Intermediate_Files/Normalizados.txt");
            PrintWriter printWriter4 = new PrintWriter(file4);



//======================================================================================================================= DISTRIBUIÇÃO



// Distribuição da população, taxas e população total

            int numClasses = vetor.length;
            double[] Nt = new double[numGeracoes + 1];
            double[] TaxasVariacao = new double[numGeracoes];
            double[][] ClassesTotal = new double[numGeracoes + 1][vetor.length];
            double[][] ClassesNorm = new double[numGeracoes + 1][vetor.length];
            distruibuicao_exe(matrizLeslie, vetor, numGeracoes, Nt, TaxasVariacao, ClassesTotal, ClassesNorm);  //distribuição população + variação


//Escrever nos ficheiros temporários

            for (int i = 0; i <= numGeracoes; i++) {                        //População total
                printWriter.println(i + " " + Nt[i]);
            }

            for (int i = 0; i < numGeracoes; i++) {                          //Taxas de variação
                printWriter2.println(i + " " + TaxasVariacao[i]);
            }


            for (int linhas = 0; linhas < ClassesTotal.length; linhas++) {      //Classes não normalizadas
                printWriter3.print(linhas + " ");
                for (int colunas = 0; colunas < ClassesTotal[0].length; colunas++) {
                    printWriter3.print(ClassesTotal[linhas][colunas] + " ");
                }
                printWriter3.print("\n");
            }
            printWriter3.println();


            for (int linhas = 0; linhas < ClassesTotal.length; linhas++) {      //Classes normalizadas
                printWriter4.print(linhas + " ");
                for (int colunas = 0; colunas < ClassesTotal[0].length; colunas++) {
                    printWriter4.print(ClassesNorm[linhas][colunas] + " ");
                }
                printWriter4.println();
            }
            printWriter.close();             //Fechar os ficheiros
            printWriter2.close();
            printWriter3.close();
            printWriter4.close();


//Valor e Vetor proprios

            String resposta;
            double[] maiorNumProprio = new double[1];
            double[] vetorProprioNormalizado = preverEvolucaoEDestribuicao(matrizLeslie, maiorNumProprio);

            if (modo.equals("Interativo")) {
                System.out.println("Deseja calcular o vetor e valor proprio?(1.Sim, 2.Nao)");
                resposta = input.next();
                while (!(resposta.equals("1") || resposta.equals("2"))) {
                    System.out.println("Resposta inválida. (1.Sim/2.Nao)");
                    resposta = input.next();
                }
                if (resposta.equals("1")) {
                    //Valor próprio
                    printWriter5.println();
                    printWriter5.println("Maior valor próprio e vetor associado");
                    printWriter5.printf("%s%.4f", "lambda=", maiorNumProprio[0]);
                    printWriter5.println();
                    System.out.println();
                    System.out.println("Maior valor próprio e vetor associado");
                    System.out.printf("%s%.4f", "lambda=", maiorNumProprio[0]);
                    System.out.println();

                    //vetor próprio
                    printWriter5.print("vetor proprio associado=(");
                    System.out.print("vetor proprio associado=(");

                    for (int i = 0; i < vetorProprioNormalizado.length; i++) {

                        if (i != vetorProprioNormalizado.length - 1) {
                            printWriter5.print(Math.round(vetorProprioNormalizado[i]*100)/100.00 + ",");
                            System.out.print(Math.round(vetorProprioNormalizado[i]*100)/100.00 + ",");
                        } else {
                            printWriter5.print(Math.round(vetorProprioNormalizado[i]*100)/100.00); // Quando o i é igual ao tamanho - 1 não metemos "," porque é o último.
                            System.out.print(Math.round(vetorProprioNormalizado[i]*100)/100.00);
                        }
                    }
                    printWriter5.print(")");
                    printWriter5.println();
                    System.out.print(")");
                    System.out.println();
                }
            } else {
                if (VVproprio) {
                    printWriter5.println();
                    printWriter5.println("Maior valor próprio e vetor associado");
                    printWriter5.printf("%s%.4f", "lambda=", maiorNumProprio[0]);
                    printWriter5.println();
                    printWriter5.print("vetor proprio associado=(");
                    for (int i = 0; i < vetorProprioNormalizado.length; i++) {

                        if (i != vetorProprioNormalizado.length - 1)
                            printWriter5.print(Math.round(vetorProprioNormalizado[i]) + ",");
                        else
                            printWriter5.print(Math.round(vetorProprioNormalizado[i])); // Quando o i é igual ao tamanho - 1 não metemos "," porque é o último.
                    }
                    printWriter5.print(")");
                    printWriter5.println();
                }
            }
            //  TimeUnit.SECONDS.sleep(1);



//====================================================================================================================== GRÁFICOS



//Modo interativo

            if (modo.equals("Interativo")) {
                String respostap2 = "1";
                boolean t = true, v = true, c = true, cn = true;

                while (respostap2.equals("1")) {
                    System.out.println("Quer ver qual gráfico? 1. População total, 2. Variacao da população, 3. Total de cada classe, 4. Total da classe normalizado, 5.sair");
                    int decisao = input.nextInt();
                    while (decisao > 5 || decisao < 1) {        //Verificador
                        System.out.println("Valor invalido.");
                        decisao = input.nextInt();
                    }
                    formato = 0;


                    //População total

                    if (decisao == 1) {
                        System.out.println("\nNumero total de individuos");
                        System.out.println("(t, Nt)");
                        if (t) {
                            printWriter5.println("\nNumero total de individuos");
                            printWriter5.println("(t, Nt)");
                        }
                        for (int i = 0; i <= numGeracoes; i++) {
                            System.out.printf("(%d, %.2f)\n", i, Nt[i]);
                            printWriter5.printf("(%d, %.2f)\n", i, Nt[i]);
                        }
                        poptotal_graph(modo, dir, formato, nome);
                        t = false;
                    }


                    //Taxa de variação

                    if (decisao == 2) {
                        if (v) {
                            printWriter5.println("\nCrescimento da população");
                            printWriter5.println("(t, delta_t)");
                        }
                        System.out.println("\nCrescimento da população");
                        System.out.println("(t, delta_t)");
                        for (int i = 0; i < numGeracoes; i++) {
                            System.out.println(i + " " + TaxasVariacao[i]);                          //Escrever no ficheiro
                            printWriter5.println("(" + i + ", " + TaxasVariacao[i] + ")");
                        }
                        variacaopop_graph(modo, dir, formato, nome);
                        v = false;
                    }


                    //Classes não normalizadas

                    if (decisao == 3) {
                        if (c) {
                            printWriter5.println("\nNumero por classe (não normalizado)");
                            printWriter5.print("(t, ");
                        }
                        System.out.println("\nNumero por classe (não normalizado)");
                        System.out.print("(t, ");
                        for (int i = 0; i < ClassesTotal[0].length; i++) {
                            if (c) printWriter5.print("x" + (i + 1));
                            System.out.print("x" + (i + 1));
                            if (i < ClassesTotal[0].length - 1) {
                                if (c) printWriter5.print(", ");
                                System.out.print(", ");
                            }
                        }
                        if (c) printWriter5.print(")\n");
                        System.out.print(")\n");
                        for (int linhas = 0; linhas < ClassesTotal.length; linhas++) {
                            System.out.print("(" + linhas + ", ");
                            if (c) printWriter5.print("(" + linhas + ", ");
                            for (int colunas = 0; colunas < ClassesTotal[0].length; colunas++) {
                                System.out.printf("%.2f", ClassesTotal[linhas][colunas]);                          //Escrever no ficheiro
                                if (c) printWriter5.printf("%.2f", ClassesTotal[linhas][colunas]);
                                if (colunas < ClassesTotal[0].length - 1) {
                                    System.out.print(", ");
                                    if (c) printWriter5.print(", ");
                                }
                            }
                            System.out.println();
                            if (c) printWriter5.print(")\n");
                        }
                        classes_graph(modo, dir, formato, numClasses, nome);
                        c = false;
                    }


                    //Classes normalizadas

                    if (decisao == 4) {
                        if (cn) {
                            printWriter5.println("\nNumero por classe (normalizado)");
                            printWriter5.print("(t, ");
                        }
                        System.out.println("\nNumero por classe (normalizado)");
                        System.out.print("(t, ");
                        for (int i = 0; i < ClassesTotal[0].length; i++) {
                            if (cn) printWriter5.print("x" + (i + 1));
                            System.out.print("x" + (i + 1));
                            if (i < ClassesTotal[0].length - 1) {
                                if (cn) printWriter5.print(", ");
                                System.out.print(", ");
                            }
                        }
                        if (cn) printWriter5.print(")\n");
                        System.out.print(")\n");
                        for (int linhas = 0; linhas < ClassesTotal.length; linhas++) {
                            if (cn) printWriter5.print("(" + linhas + ", ");
                            System.out.print("(" + linhas + ", ");

                            for (int colunas = 0; colunas < ClassesTotal[0].length; colunas++) {
                                System.out.printf("%.2f", ClassesNorm[linhas][colunas]);
                                if (cn) printWriter5.printf("%.2f", ClassesNorm[linhas][colunas]);
                                if (colunas < ClassesTotal[0].length - 1) {
                                    if (cn) printWriter5.print(", ");
                                    System.out.print(", ");
                                }
                            }
                            if (cn) printWriter5.print(")\n");
                            System.out.print(")\n");
                        }

                        normalizados_graph(modo, dir, formato, numClasses, nome);
                        cn = false;
                    }


                    //Repetir?

                    if (decisao<5) {
                        System.out.println("Quer ver mais gráficos?(1.Sim/2.Nao)");
                        respostap2 = input.next();
                        while (!(respostap2.equals("1") || respostap2.equals("2") )) {
                            System.out.println("Resposta invalida.(1.Sim/2.Nao)");
                            respostap2 = input.next();
                        }
                    }
                    else respostap2="2";
                }


//Modo nao interativo

            } else {

                if (dimensao_pop) {        //Dimensão total
                    poptotal_graph(modo, dir, formato, nome);
                    printWriter5.println("\nNumero total de individuos");
                    printWriter5.println("(t, Nt)");

                    for (int i = 0; i <= numGeracoes; i++) {                     //Nt
                        printWriter5.printf("(%d, %.2f)\n", i, Nt[i]);
                    }
                }

                if (variacao_pop) {      //Variação da população
                    printWriter5.println("\nCrescimento da população");
                    printWriter5.println("(t, delta_t)");
                    for (int i = 0; i < numGeracoes; i++) {
                        printWriter5.println("(" + i + ", " + TaxasVariacao[i] + ")");
                    }
                    variacaopop_graph(modo, dir, formato, nome);
                }


                //Classes não normalizadas

                printWriter5.println("\nNumero por classe (não normalizado)");
                printWriter5.print("(t, ");
                for (int i = 0; i < ClassesTotal[0].length; i++) {
                    printWriter5.print("x" + (i + 1));
                    if (i < ClassesTotal[0].length - 1) {
                        printWriter5.print(", ");
                    }
                }
                printWriter5.print(")\n");
                for (int linhas = 0; linhas < ClassesTotal.length; linhas++) {
                    printWriter5.print("(" + linhas + ", ");
                    for (int colunas = 0; colunas < ClassesTotal[0].length; colunas++) {
                        printWriter5.printf("%.2f", ClassesTotal[linhas][colunas]);
                        if (colunas < ClassesTotal[0].length - 1) {
                            printWriter5.print(", ");
                        }
                    }
                    printWriter5.print(")\n");
                }
                classes_graph(modo, dir, formato, numClasses, nome);


                //Classes normalizadas

                printWriter5.println("\nNumero por classe (normalizado)");
                printWriter5.print("(t, ");

                for (int i = 0; i < ClassesTotal[0].length; i++) {
                    printWriter5.print("x" + (i + 1));
                    if (i < ClassesTotal[0].length - 1) {
                        printWriter5.print(", ");
                    }
                }
                printWriter5.print(")\n");
                for (int linhas = 0; linhas < ClassesTotal.length; linhas++) {
                    printWriter5.print("(" + linhas + ", ");

                    for (int colunas = 0; colunas < ClassesTotal[0].length; colunas++) {
                        printWriter5.printf("%.2f", ClassesNorm[linhas][colunas]);
                        if (colunas < ClassesTotal[0].length - 1) {
                            printWriter5.print(", ");
                        }
                    }
                    printWriter5.print(")\n");
                }
                normalizados_graph(modo, dir, formato, numClasses, nome);
            }
            printWriter5.close();


//Apagar diretorio dos ficheiros intermedios

            File pasta = new File("./Intermediate_Files");
            deleteFile(pasta);


            if (modo.equals("Interativo")) {
                System.out.println("Deseja guardar os resultados num ficheiro de saída?(1.Sim/2.Nao)");
                resposta = input.nextLine();
                while (!(resposta.equals("1") || resposta.equals("2"))) {
                    System.out.println("Resposta inválida.");
                    resposta = input.nextLine();
                }
                if (resposta.equals("2")) deleteFile(Ficheiro_Saida);
                System.out.println("Deseja executar novamente o programa?(1.Sim/2.Nao)");
                repetir = input.nextLine();
                while (!(repetir.equals("1") || repetir.equals("2"))) {
                    System.out.println("Resposta inválida.");
                    repetir = input.nextLine();
                }
            }
            ficheiro = false;
        } while ((modo.equals("Interativo")) && (repetir.equals("1")));
    }



//                                        ---   MODULOS ---



//======================================================================================================================= DISTRIBUIÇÃO, TAXAS  E POP TOTAL



//Método: População total e distribuição etc.

    public static void distruibuicao_exe(double[][] MatrizLeslei, double[][] Pop, int geracoes, double[] Nt, double[] TaxasVariacao, double[][] ClassesTotal, double[][] ClassesNorm) {            //Aqui começa a executar todas as partes do tema
        int t = 0;
        double[][] calculo = Pop;
        double total1;
        total1 = calculoTotal(calculo);
        Nt[0] = total1;
        distribuicao(calculo, total1, t, ClassesTotal, ClassesNorm);
        if (geracoes > 0) {
            calculo = multiplicarMatrizes(MatrizLeslei, calculo);
            taxaVariacao(calculo, total1, t, TaxasVariacao);
        }
        if (geracoes >= 1) {
            calculo = multiplicarMatrizes(MatrizLeslei, Pop);
            for (int i = 2; i <= geracoes; i++) {                                              //repete o numero de gerações, começa no 2, pq k^1=k
                t++;
                total1 = calculoTotal(calculo);                                         //população total ( N(t) )
                Nt[t] = total1;
                distribuicao(calculo, total1, t, ClassesTotal, ClassesNorm);                                        //Distruibuição da população
                calculo = multiplicarMatrizes(MatrizLeslei, calculo);
                taxaVariacao(calculo, total1, t, TaxasVariacao);                                //atualizar resultado para t+1
            }
            total1 = calculoTotal(calculo);
            t++;
            Nt[t] = total1;
            distribuicao(calculo, total1, t, ClassesTotal, ClassesNorm);
        }

    }


//Método:Calculo das taxas de variação

    public static void taxaVariacao(double[][] calculo, double total, int t, double[] TaxasVariacao) {
        double total2 = calculoTotal(calculo);                                    //população total momento seguinte ------>     N(t+1)
        double taxa = total2 / total;
        double delta_t = round(taxa);
        TaxasVariacao[t] = delta_t;
    }


//Método:Calculo da população total

    public static double calculoTotal(double[][] calculo) {
        double total = 0;
        for (int i = 0; i < calculo.length; i++) {
            total += calculo[i][0];                 //Vai somando os valores do array das amostras
        }
        return total;
    }


//Método:Distribuição da população

    public static void distribuicao(double[][] calculo, double total, int t, double[][] ClassesTotal, double[][] ClassesNorm) {
        for (int i = 0; i < calculo.length; i++) {
            double resultado = ((calculo[i][0] / total) * 100);
            ClassesTotal[t][i] = calculo[i][0];
            ClassesNorm[t][i] = resultado;
        }
    }


//Método: Multiplicação de Matrizes

    public static double[][] multiplicarMatrizes(double[][] arr1, double[][] arr2) {
        double[][] resultado = new double[arr1.length][arr2[0].length];
        for (int linha = 0; linha < arr1.length; linha++) {
            for (int coluna = 0; coluna < arr2[0].length; coluna++) {
                resultado[linha][coluna] = 0;
                for (int k = 0; k < arr2.length; k++) {
                    resultado[linha][coluna] += arr1[linha][k] * arr2[k][coluna];  //Multiplica uma linha pela coluna da outra
                }
            }
        }
        return resultado;
    }

//Método:Arredondar Valores

    public static double round(double value) {
        return Math.round(value * 100.00) / 100.00;
    }


//Método:Mostra a Matriz

    public static void mostrarMatriz(double[][] arr) {
        for (int linha = 0; linha < arr.length; linha++) {
            for (int coluna = 0; coluna < arr[0].length; coluna++) {
                System.out.print(arr[linha][coluna] + " ");        //Arredondamento duas casas decimais
            }
            System.out.println();
        }
    }


//Método:Apagar pasta+conteudos

    public static void deleteFile(File element) {
        if (element.isDirectory()) {
            for (File sub : Objects.requireNonNull(element.listFiles())) {
                deleteFile(sub);
            }
        }
        element.delete();
    }



//====================================================================================================================== LEITURA DE DADOS



//Método:Posição do igual (=)

    public static int posicaoIgual(String string) { // Descubro em que posição está o igual porque só preciso
        // do que está para a frente dele;
        int contador = 0;
        char[] array = string.toCharArray(); // Passo cada caracter da String para um Array

        for (int i = 0; i < array.length; i++) {

            String str = String.valueOf(array[i]);

            if (!str.equals("=")) contador++;
            else {
                contador++;
                return contador;
            }
        }
        return contador;
    }


//Método:Leitura do Ficheiro

    public static boolean lerFicheiro(double[][] vetorFinal, double[] taxaSiFinal, double[] taxaFiFinal, String nomeFicheiro) throws FileNotFoundException {
        Scanner in = new Scanner(new File(nomeFicheiro));
        int[] posicao = new int[200];
        String line = in.nextLine(); // Primeiro seleciona-se a primeira linha
        while (line.isEmpty()) {    // Se não tiver nada, vai estar sempre a avançar
            line = in.nextLine();  // para a próxima até encontrar alguma
        }


        for (int k = 1; k <= 3; k++) { // Vou ler 3 dados

            String[] linhaAtual = line.split(",");

            if (linhaAtual[0].startsWith("x")) { // Se achar o x, sei que estou a ler os valores do Vetor;

                String[] itensVetor = line.split(", "); // Criar um array em que cada valor está separado por ", ";

                for (int i = 0; i < vetorFinal.length; i++) {
                    posicao[i] = Integer.parseInt(itensVetor[i].substring(1, posicaoIgual(itensVetor[i]) - 1));
                    itensVetor[i] = itensVetor[i].substring(posicaoIgual(itensVetor[i])); // Uso o substring para tirar tudo o que não sejam o resultado;
                    vetorFinal[i][0] = Integer.parseInt(itensVetor[i]); // Passo o valor de String para Int;
                    if (i > 0) {
                        if ((posicao[i] <= posicao[i - 1]) || (posicao[i] != posicao[i - 1] + 1)) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    } else {
                        if (posicao[i] != 0) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    }
                }
            }

            if (linhaAtual[0].startsWith("s")) { // Se achar o s, sei que estou a ler os valores da TaxaSi;

                String[] itensSi = line.split(", ");

                for (int i = 0; i < taxaSiFinal.length; i++) {
                    posicao[i] = Integer.parseInt(itensSi[i].substring(1, posicaoIgual(itensSi[i]) - 1));
                    itensSi[i] = itensSi[i].substring(posicaoIgual(itensSi[i]));
                    taxaSiFinal[i] = Double.parseDouble(itensSi[i]); // Passo o valor de String para Double;

                    if (i > 0) {
                        if ((posicao[i] <= posicao[i - 1]) || (posicao[i] != posicao[i - 1] + 1)) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    } else {
                        if (posicao[i] != 0) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    }

                    if (taxaSiFinal[i] <= 0 || taxaSiFinal[i] >= 1) {  // o Valor tem de estar entre 0 e 1 (exclusive);
                        System.out.println("Valor inválido");
                        return false;
                    }

                }
            }

            if (linhaAtual[0].startsWith("f")) {  // Se achar o f, sei que estou a ler os valores da TaxaFi;
                String[] itensFi = line.split(", ");

                for (int i = 0; i < taxaFiFinal.length; i++) {

                    posicao[i] = Integer.parseInt(itensFi[i].substring(1, posicaoIgual(itensFi[i]) - 1));
                    itensFi[i] = itensFi[i].substring(posicaoIgual(itensFi[i]));
                    taxaFiFinal[i] = Double.parseDouble(itensFi[i]);
                    if (i > 0) {
                        if ((posicao[i] <= posicao[i - 1]) || (posicao[i] != posicao[i - 1] + 1)) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    } else {
                        if (posicao[i] != 0) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    }

                    if (taxaFiFinal[i] < 0) { // O valor tem de ser maior que 0 (inclusive)
                        System.out.println("Valor Inválido");
                        return false;
                    }
                }
            }
            if (k != 3) {                               // Se eu estiver na terceira iteração
                line = in.nextLine();      // significa que não vai ter mais nada pra ler
                while (line.isEmpty()) {              // depois;
                    line = in.nextLine();            // Se eu estiver nas duas primeiras tenho que ler
                }                                   // a próxima linha.
            }

        }
        in.close();
        return true;
    }


//Método:Calcular Numero de valores

    public static int calcularNumValores(String nomeFicheiro) throws FileNotFoundException {  // Vejo na primeira linha com conteúdo
        Scanner in = new Scanner(new File(nomeFicheiro));                    // o tamanho do array de tudo
        String line = in.nextLine();                                     // que está separado por ", "
        while (line.isEmpty()) {
            line = in.nextLine();
        }
        String[] itens = line.split(", ");

        if (itens[0].startsWith("s")) return (itens.length + 1); // Se a string começar por s, sei que estou
        // a lidar com a taxaSi logo o tamanho é mais 1
        return itens.length;
    }




//Método: Converter para Matriz de Leslei

    public static double[][] atribuirMatrizLeslie(double[] taxaSi, double[] taxaFi) {

        int tamanho = taxaFi.length;
        double[][] matrizLeslie = new double[tamanho][tamanho];


        for (int coluna = 0; coluna < tamanho; coluna++) {
            matrizLeslie[0][coluna] = taxaFi[coluna];
        }

        int coluna = 0;
        int linha = 1;

        while (linha < tamanho) {

            matrizLeslie[linha][coluna] = taxaSi[coluna];
            coluna++;
            linha++;
        }
        return matrizLeslie;
    }




//Método: Leitura dos valores no modo Interativo (sem ficheiro)

    public static boolean lerVetoresEnormes(double[][] vetorEnorme, double[] taxaSiEnorme, double[] taxaFiEnorme) {

        String resposta;
        int i = 0;
        tamanhoSemFicheiro = 0;


        System.out.println("Deseja começar a inserir valores ou o nome de um ficheiro? (1.Valor)(2.Ficheiro)");
        resposta = input.next();
        while (!resposta.equals("1") && !resposta.equals("2")) {

            System.out.println("Resposta inválida, insira '1' ou '2':");
            resposta = input.next();
            input.nextLine();
        }

        if (resposta.equals("2")) return false;

        System.out.println("Pode inserir um nome de um ficheiro depois de inserir um valor.");


        do {
            System.out.println("Insira o valor de x0" + i);
            vetorEnorme[i][0] = input.nextInt();
            while (vetorEnorme[i][0] < 0) {

                System.out.println("Número inválido,insira novamente:");
                vetorEnorme[i][0] = input.nextInt();
            }

            i++;
            tamanhoSemFicheiro++;

            System.out.println("Deseja continuar a ler valores? (1.Sim)(2.Nao)(3.Ficheiro)");
            resposta = input.next();
            input.nextLine();

            while (!resposta.equals("1") && !resposta.equals("2") && !resposta.equals("3")) {

                System.out.println("Resposta inválida, insira '1' ou '2' ou '3':");
                resposta = input.next();
                input.nextLine();
            }

            if (resposta.equals("3")) return false;

        } while (resposta.equals("1"));

        for (int j = 0; j < tamanhoSemFicheiro - 1; j++) {


            System.out.println("Insira o valor de s0" + j);
            taxaSiEnorme[j] = input.nextDouble();

            while (taxaSiEnorme[j] <= 0 || taxaSiEnorme[j] >= 1) {

                System.out.println("Número inválido,insira novamente:");
                taxaSiEnorme[j] = input.nextDouble();
            }

            System.out.println("Deseja continuar a ler valores? (1.Sim)(2.Ficheiro)");
            resposta = input.next();
            input.nextLine();

            while (!resposta.equals("1") && !resposta.equals("2")) {

                System.out.println("Resposta inválida, insira '1' ou '2':");
                resposta = input.next();
                input.nextLine();
            }
            if (resposta.equals("2")) return false;
        }

        for (int k = 0; k < tamanhoSemFicheiro; k++) {

            System.out.println("Insira o valor de f0" + k);
            taxaFiEnorme[k] = input.nextDouble();

            while (taxaFiEnorme[k] < 0) {
                System.out.println("Número inválido,insira novamente:");
                taxaFiEnorme[k] = input.nextDouble();
            }

            if (k != tamanhoSemFicheiro - 1) {

                System.out.println("Deseja continuar a ler valores? (1.Sim)(2.Ficheiro)");
                resposta = input.next();
                input.nextLine();

                while (!resposta.equals("1") && !resposta.equals("2")) {

                    System.out.println("Resposta inválida, insira '1' ou '2':");
                    resposta = input.next();
                    input.nextLine();
                }
            }

            if (resposta.equals("2")) return false;
        }


        return true;
    }


//Método: Converter para vetor

    public static double[][] passarVetorEnormeNormal(double[][] vetor) {

        double[][] vetorFinal = new double[tamanhoSemFicheiro][1];

        for (int i = 0; i < tamanhoSemFicheiro; i++) {

            vetorFinal[i][0] = vetor[i][0];
        }
        return vetorFinal;
    }

 //===================================================================================================================== VALOR E VETOR PRORIO



 //Método: Prever Evolução e Distribuição

    public static double[] preverEvolucaoEDestribuicao(double[][] matrizLeslie, double[] maiorNumProprio) {


        Matrix matrizMatrix = new Basic2DMatrix(matrizLeslie);             // Cria uma matriz.
        EigenDecompositor eigenD = new EigenDecompositor(matrizMatrix);

        Matrix[] matD = eigenD.decompose();    //Obtem valores e vetores próprios fazendo "Eigen Decomposition".


        double[][] matrizVetores = matD[0].toDenseMatrix().toArray();       // Converte objeto Matrix (duas matrizes)
        double[][] matrizValores = matD[1].toDenseMatrix().toArray();      // para array Java;

        for (int linha = 0; linha < matrizVetores.length; linha++) {                                               // Vou percorar as duas matrizes
            for (int coluna = 0; coluna < matrizVetores.length; coluna++) {                                       // e vou arredondar tudo a duas casas
                                                                                                                 // decimais (nota: Não sei porque mas se
                matrizValores[linha][coluna] = Math.round(matrizValores[linha][coluna] * 10000) / 10000.00;     // dividir por 100 inteiro não resulta por
                matrizVetores[linha][coluna] = Math.round(matrizVetores[linha][coluna] * 100) / 100.00;    // isso é essencial ser 100.00).
                // System.out.println(matrizValores[linha][coluna]);
            }
        }

        maiorNumProprio[0] = procurarMaiorValorProprio(matrizValores);


        // preverEvolucaoEspecie(maiorNumProprio[0], printWriter5);
        // preverDistruibuicaoFinal(distruibuicao, printWriter5);
        return distribuicaoFinal(matrizVetores, matrizValores); // ====               //double[] distruibuicao = distribuicaoFinal(matrizVetores, matrizValores);

    }

 /*   public static void preverDistruibuicaoFinal(double[] vetorProprioNormalizado, PrintWriter printWriter5) {  // Isto serve só pra printar.

        printWriter5.print("vetor proprio associado=(");
        for (int i = 0; i < vetorProprioNormalizado.length; i++) {


            if (i != vetorProprioNormalizado.length - 1)
                printWriter5.print(Math.round(vetorProprioNormalizado[i]) + ",");
            else
                printWriter5.print(Math.round(vetorProprioNormalizado[i])); // Quando o i é igual ao tamanho - 1 não metemos "," porque é o último.
        }
        printWriter5.print(")");
        printWriter5.println();
    }*/


//Método: Distribuição Final

    public static double[] distribuicaoFinal(double[][] vetorProprio, double[][] valorProprio) { // Para calcular vetor próprio associado.

        int coluna = colunaMaiorNum(valorProprio), tamanho = vetorProprio.length;
        double soma = 0,somaDistribuicao = 0;
        double[] distribuicao = new double[tamanho];

        for (int linha = 0;linha < tamanho;linha++){
            vetorProprio[linha][coluna] = (Math.round(vetorProprio[linha][coluna]*100) /100.00);

        }


        for (int linha = 0; linha < tamanho; linha++) { // Primeiro vou calcular a soma da coluna do maior valor próprio,
            soma = vetorProprio[linha][coluna] + soma;   // preciso do valor soma para normalizar o vetor.
        }

        for (int linha = 0; linha < tamanho; linha++) {
            distribuicao[linha] = (vetorProprio[linha][coluna] / soma); // Coloco os valores da primeira coluna noutro array.
        }

        for (int linha = 0; linha < tamanho; linha ++){
            distribuicao[linha]= Math.round(distribuicao[linha]*10000)/100.00;
        }
        
        return distribuicao;
    }


//Método: Procurar maior valor próprio

    public static double procurarMaiorValorProprio(double[][] matriz) { // Procuro na matriz o maior numero que será
        // o maior valor próprio
        double maiorNum = matriz[0][0];
        int tamanho = matriz.length;

        for (int linha = 0; linha < tamanho; linha++) {
            for (int coluna = 0; coluna < tamanho; coluna++) {
                if (matriz[linha][coluna] > maiorNum) maiorNum = matriz[linha][coluna];
            }
        }
        return maiorNum;
    }

    public static boolean test_colunaMaiorNum(double[][] matriz, int expectedNum) {

        return colunaMaiorNum(matriz) == expectedNum;
    }


//Método: coluna dp maior valor próprio

    public static int colunaMaiorNum(double[][] matriz) { // Procuro em que coluna está o maior numero

        double maiorNum;
        int maiorColuna, tamanho;

        maiorNum = matriz[0][0];
        maiorColuna = 0;
        tamanho = matriz.length;

        for (int linha = 0; linha < tamanho; linha++) {
            for (int coluna = 0; coluna < tamanho; coluna++) {

                if (matriz[linha][coluna] > maiorNum) {

                    maiorNum = matriz[linha][coluna];
                    maiorColuna = coluna;
                }
            }
        }

        return maiorColuna;
    }

   /* public static void preverEvolucaoEspecie(double valorProprio, PrintWriter printWriter5) {
        printWriter5.println();
        printWriter5.println("Maior valor próprio e vetor associado");
        printWriter5.printf("%s%.4f", "lambda=", valorProprio);
        printWriter5.println();
    }*/


//Método: Cópia manual de array

    public static double[] passarTaxaSiEnormeNormal(double[] taxa) {

        double[] taxaFinal = new double[tamanhoSemFicheiro - 1];

        for (int i = 0; i < tamanhoSemFicheiro - 1; i++) {

            taxaFinal[i] = taxa[i];
        }

        return taxaFinal;
    }


//Método: Cópia manual de array

    public static double[] passarTaxaFiEnormeNormal(double[] taxa) {

        double[] taxaFinal = new double[tamanhoSemFicheiro];

        for (int i = 0; i < tamanhoSemFicheiro; i++) {

            taxaFinal[i] = taxa[i];
        }

        return taxaFinal;
    }



//======================================================================================================================= GRÁFICOS



//Método: Gráfico da População Total

    public static void poptotal_graph(String modo, String dir, int formato, String nome) throws IOException, InterruptedException {

    //Cria ficheiro .gp

        File file = new File("./Intermediate_Files/Poptotal.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter = new PrintWriter(file);


    //Escreve o script no ficheiro

        printWriter.println("set terminal png");
        printWriter.println("set output \"Intermediate_Files/populacaototal.png\"");       //instruções para o gnuplot
        printWriter.println("set title \"População total ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"População/unidade\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n" +
                "plot 'Intermediate_Files/Populacao_total.txt' with lines title 'População Total'");
        printWriter.close();


    //Executa o ficheiro

        Runtime rt = Runtime.getRuntime();         //executar ficheiro .gp
        rt.exec("wgnuplot Intermediate_Files/Poptotal.gp");


        boolean b;
        String tipo1 = "";
        String tipo2 = "";
        if (modo.equals("Interativo")) {


    //Abre a imagem

            TimeUnit.SECONDS.sleep(1);
            System.out.println("(Gráfico aberto)");
            File f = new File("Intermediate_Files/populacaototal.png");
            Desktop d = Desktop.getDesktop();
            d.open(f);


    //Guardar?

            System.out.println("Quer guardar este gráfico?(1.Sim)(2.Nao)");
            String resposta = input.next();
            while (!(resposta.equals("1") || resposta.equals("2"))) {
                System.out.println("Resposta invalida.(1.Sim/2.Nao)");
                resposta = input.next();
            }


    //Formato

            b = resposta.equals("1");
            if (b) {
                System.out.println("Que formato quer para gravar o seu gráfico?");
                System.out.println("1:png 2:txt 3:eps");
                formato = input.nextInt();
                while ((formato < 1) || (formato >= 4)) {
                    System.out.println("Os seus valores não são válidos, por favor insira números entre 1 e 3");
                    formato = input.nextInt();
                }
            }
        } else b = true;

        //é defenido o formato em que o utilizador quer gravar a imagem
        if (formato == 1) {
            tipo1 = "png";
            tipo2 = "png;";
        }
        if (formato == 2) {
            tipo1 = "txt";
            tipo2 = "dumb";
        }
        if (formato == 3) {
            tipo1 = "eps";
            tipo2 = "postscript eps";
        }


    //Cria novo ficheiro .gp com o formato desejado

        File file1 = new File("./Intermediate_Files/Poptotal.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter1 = new PrintWriter(file1);
        printWriter1.println("set terminal " + tipo2);
        printWriter1.println("set output \"" + dir + "/Graficos/" + nome + "_" + "Graph_PopulacaoTotal." + tipo1 + "\"");
        printWriter1.println("set title \"População total ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"População/unidade\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n" +
                "plot 'Intermediate_Files/Populacao_total.txt' with lines title 'População Total'");


        printWriter1.close();


    //Executa no caso de querer gravar

        if (b) {
            Runtime rt1 = Runtime.getRuntime();
            rt1.exec("wgnuplot ./Intermediate_Files\\Poptotal.gp");
        }                                  //a imagem é aberta no ecrã através do ficheiro .png que já tinha sido criado

    }


//Método: Gráfico da Variação da População

    public static void variacaopop_graph(String modo, String dir, int formato, String nome) throws IOException, InterruptedException {

    //Cria ficheiro .gp

        File file = new File("./Intermediate_Files/variacaopop.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter = new PrintWriter(file);


    //Escreve o script no ficheiro

        printWriter.println("set terminal png");
        printWriter.println("set output \"./Intermediate_Files/variacaopop.png");       //instruções para o gnuplot
        printWriter.println("set title \"Variação da população ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Variação\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n" +
                "plot 'Intermediate_Files/Variacao.txt' with lines title 'Variação'");
        printWriter.close();


    //Executa o ficheiro

        Runtime rt = Runtime.getRuntime();         //executar ficheiro .gp
        rt.exec("wgnuplot ./Intermediate_Files\\variacaopop.gp");

        boolean b;
        String tipo1 = "";
        String tipo2 = "";
        if (modo.equals("Interativo")) {
    //Abre a imagem

            TimeUnit.SECONDS.sleep(1);
            System.out.println("(Gráfico aberto)");
            File f = new File("./Intermediate_Files/variacaopop.png");
            Desktop d = Desktop.getDesktop();
            d.open(f);


    //Guardar?

            System.out.println("Quer guardar este gráfico?(1.Sim)(2.Nao)");
            String resposta = input.next();
            while (!(resposta.equals("1") || resposta.equals("2"))) {
                System.out.println("Resposta invalida.(1.Sim/2.Nao)");
                resposta = input.next();
            }


    //Formato

            b = resposta.equals("1");
            if (b) {
                System.out.println("Que formato quer para gravar o seu gráfico?");
                System.out.println("1:png 2:txt 3:eps");
                formato = input.nextInt();
                while ((formato < 1) || (formato >= 4)) {
                    System.out.println("Os seus valores não são válidos, por favor insira números entre 1 e 3");
                    formato = input.nextInt();
                }
            }
        } else b = true;
        //é defenido o formato em que o utilizador quer gravar a imagem
        if (formato == 1) {
            tipo1 = "png";
            tipo2 = "png;";
        }
        if (formato == 2) {
            tipo1 = "txt";
            tipo2 = "dumb";
        }
        if (formato == 3) {
            tipo1 = "eps";
            tipo2 = "postscript eps enhanced color font 'Helvetica,10'";
        }


    //Cria novo ficheiro .gp com o formato desejado

        File file1 = new File("./Intermediate_Files/variacaopop.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter1 = new PrintWriter(file1);
        printWriter1.println("set terminal " + tipo2);
        printWriter1.println("set output \"" + dir + "/Graficos/" + nome + "_" + "Graph_TaxasDeVariacao." + tipo1 + "\"");
        printWriter1.println("set title \"Variação ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Variação\"\n" +


                "\n" +
                "set style data linespoints\n" +
                "\n" +
                "plot 'Intermediate_Files/Variacao.txt' with lines title 'Variação'");


        printWriter1.close();
    //Executa no caso de querer gravar
        if (b) {
            Runtime rt1 = Runtime.getRuntime();
            rt1.exec("wgnuplot ./Intermediate_Files\\variacaopop.gp");
        }                                  //a imagem é aberta no ecrã através do ficheiro .png que já tinha sido criado

    }


//Método: Gráfico das Classes Não Normalizadas

    public static void classes_graph(String modo, String dir, int formato, int numclasses, String nome) throws IOException, InterruptedException {

    //Cria ficheiro .gp
        File file = new File("./Intermediate_Files/Classes.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter = new PrintWriter(file);

    //Escreve o script no ficheiro
        printWriter.println("set terminal png");
        printWriter.println("set output \"./Intermediate_Files/Classes.png");       //instruções para o gnuplot
        printWriter.println("set title \"Classes ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Classes\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n");
        printWriter.print("plot ");
        for (int i = 2; i <= numclasses + 1; i++) {

            printWriter.print("'./Intermediate_Files/Classes.txt' using 1:" + i + " with lines title 'Classe " + (i - 1) + "',");

        }
        printWriter.close();


    //Executa o ficheiro

        Runtime rt = Runtime.getRuntime();         //executar ficheiro .gp
        rt.exec("wgnuplot ./Intermediate_Files\\Classes.gp");

        String resposta;
        String tipo1 = "";
        String tipo2 = "";
        boolean b;
        if (modo.equals("Interativo")) {
    //Abre a imagem
            TimeUnit.SECONDS.sleep(1);               //Delay para executar o script e criar a imagem corretamente
            System.out.println("(Gráfico aberto)");
            File f = new File("./Intermediate_Files/Classes.png");
            Desktop d = Desktop.getDesktop();
            d.open(f);


    //Guardar?
            System.out.println("Quer guardar este gráfico?(1.Sim)(2.Nao)");
            resposta = input.next();
            while (!(resposta.equals("1") || resposta.equals("2"))) {
                System.out.println("Resposta invalida.(1.Sim/2.Nao)");
                resposta = input.next();
            }


    //Formato

            b = resposta.equals("1");
            if (b) {
                System.out.println("Que formato quer para gravar o seu gráfico?");
                System.out.println("1:png 2:txt 3:eps");
                formato = input.nextInt();
                while ((formato < 1) || (formato >= 4)) {
                    System.out.println("Os seus valores não são válidos, por favor insira números entre 1 e 3");
                    formato = input.nextInt();
                }
            }
        } else b = true;
        //é defenido o formato em que o utilizador quer gravar a imagem
        if (formato == 1) {
            tipo1 = "png";
            tipo2 = "png;";
        }
        if (formato == 2) {
            tipo1 = "txt";
            tipo2 = "dumb";
        }
        if (formato == 3) {
            tipo1 = "eps";
            tipo2 = "postscript eps enhanced color font 'Helvetica,10'";
        }


    //Cria novo ficheiro .gp com o formato desejado
        File file1 = new File("./Intermediate_Files/Classes.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter1 = new PrintWriter(file1);
        printWriter1.println("set terminal " + tipo2);
        printWriter1.println("set output \"" + dir + "/Graficos/" + nome + "_" + "Graph_ClassesTotais." + tipo1 + "\"");
        printWriter1.println("set title \"Classes ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Classes\"\n" +


                "\n" +
                "set style data linespoints\n" +
                "\n");
        printWriter1.print("plot ");
        for (int i = 2; i <= numclasses + 1; i++) {

            printWriter1.print("'./Intermediate_Files/Classes.txt' using 1:" + i + " with lines title 'Classe " + (i - 1) + "', ");
        }


        printWriter1.close();

    //Executa no caso de querer gravar
        if (b) {
            Runtime rt1 = Runtime.getRuntime();
            rt1.exec("wgnuplot ./Intermediate_Files\\Classes.gp");
        }                                  //a imagem é aberta no ecrã através do ficheiro .png que já tinha sido criado

    }


//Método: Gráfico das Classes Normalizadas

    public static void normalizados_graph(String modo, String dir, int formato, int numclasses, String nome) throws IOException, InterruptedException {

    //Criar ficheiro .gp

        File file = new File("./Intermediate_Files/Normalizados.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter = new PrintWriter(file);


    //Script gnuplot

        printWriter.println("set terminal png");
        printWriter.println("set output \"./Intermediate_Files/Normalizados.png");       //instruções para o gnuplot
        printWriter.println("set title \"Classes ao longo do tempo em percentagem\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Classes em percentagem\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n");
        printWriter.print("plot ");
        for (int i = 2; i <= numclasses + 1; i++) {
            printWriter.print("'./Intermediate_Files/Normalizados.txt' using 1:" + i + " with lines title 'Classe " + (i - 1) + "',");
        }
        printWriter.close();


    //Executar Script gnuplot

        Runtime rt = Runtime.getRuntime();         //executar ficheiro .gp
        rt.exec("wgnuplot ./Intermediate_Files\\Normalizados.gp");


        String tipo1 = "";
        String tipo2 = "";
        boolean b;
        if (modo.equals("Interativo")) {
    //Abrir imagem .png inicial

            TimeUnit.SECONDS.sleep(1);   //Para dar tempo para processar o script e criar imagem
            File f = new File("./Intermediate_Files/Normalizados.png");
            Desktop d = Desktop.getDesktop();
            d.open(f);
            System.out.println("(Gráfico aberto)");

    //Quer gravar ?
            System.out.println("Quer guardar este gráfico?(1.Sim)(2.Nao)");
            String resposta = input.next();
            while (!(resposta.equals("1") || resposta.equals("2"))) {
                System.out.println("Resposta invalida.(1.Sim/2.Nao)");
                resposta = input.next();
            }
            b = resposta.equals("1");
            if (b) {
                //formato da imagem
                System.out.println("Que formato quer para gravar o seu gráfico?");
                System.out.println("1:png 2:txt 3:eps");
                formato = input.nextInt();
                while ((formato < 1) || (formato > 3)) {
                    System.out.println("Os seus valores não são válidos, por favor insira números entre 1 e 3");
                    formato = input.nextInt();
                }
            }
        } else b = true;
        //é defenido o formato em que o utilizador quer gravar a imagem
        if (formato == 1) {
            tipo1 = "png";
            tipo2 = "png;";
        }
        if (formato == 2) {
            tipo1 = "txt";
            tipo2 = "dumb";
        }
        if (formato == 3) {
            tipo1 = "eps";
            tipo2 = "postscript eps enhanced color font 'Helvetica,10'";
        }


    //Criar novo script para o formato desejado

        File file1 = new File("./Intermediate_Files/Normalizados.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter1 = new PrintWriter(file1);
        printWriter1.println("set terminal " + tipo2);
        printWriter1.println("set output \"" + dir + "/Graficos/" + nome + "_" + "Graph_ClassesNormalizadas." + tipo1 + "\"");
        printWriter1.println("set title \"Classes ao longo do tempo em percentagem\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Classes em percentagem\"\n" +


                "\n" +
                "set style data linespoints\n" +
                "\n");
        printWriter1.print("plot ");
        for (int i = 2; i <= numclasses + 1; i++) {

            printWriter1.print("'./Intermediate_Files/Normalizados.txt' using 1:" + i + " with lines title 'Classe " + (i - 1) + "', ");
        }


        printWriter1.close();

    //Executar o script criado
        if (b) {
            Runtime rt1 = Runtime.getRuntime();
            rt1.exec("wgnuplot ./Intermediate_Files\\Normalizados.gp");
        }                                  //a imagem é aberta no ecrã através do ficheiro .png que já tinha sido criado
    }
}






