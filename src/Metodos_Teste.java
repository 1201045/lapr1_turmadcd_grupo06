import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Metodos_Teste {

    //Variaveis globais
    static Scanner input = new Scanner(System.in);
    static int tamanhoSemFicheiro = 0;


    public static void main(String[] args) {

        double[] Nt = new double[1];
        double[] TaxasVariacao = new double[1];


        int teste = 1;
        if (teste == 1) {
            System.out.println();
            System.out.println("Teste 1-Taxa Variacao");
            double[] Expected_TaxasVariacao = {1.2};
            double[][] matriz = {{1080}, {240}, {120}};
            System.out.println("Estado: " + test_taxaVariacao(matriz, 1200, 0, TaxasVariacao, Expected_TaxasVariacao));
        }

        teste = 2;
        if (teste == 2) {
            System.out.println();
            System.out.println("Teste 2- Calculo Total");
            double[][] matriz = {{400}, {400}, {400}};
            System.out.println("Estado: " + test_calculoTotal(matriz, 1200));
        }
        teste = 3;
        if (teste == 3) {
            System.out.println();
            System.out.println("Teste 3- Distribuicao normalizada e nao normalizada");
            double[][] matriz = {{400}, {400}, {400}};
            double[][] Expected_DTotal = {{400, 400, 400}};
            double[][] Expected_DNormal = {{33.33, 33.33, 33.33}};
            double total = 1200;
            int t = 0;
            double[][] ClassesTotal = new double[1][matriz.length];
            double[][] ClassesNorm = new double[1][matriz.length];
            System.out.println("Estado: " + test_distribuicao(matriz, total, t, ClassesTotal, ClassesNorm, Expected_DTotal, Expected_DNormal));
        }
        teste = 4;
        if (teste == 4) {
            System.out.println();
            System.out.println("Teste 4-Multiplicar Matrizes");
            double[][] matriz1 = {{1.0, 1.0, 1.0}, {2.0, 2.0, 2.0}, {3.0, 3.0, 3.0}};

            double[][] matriz2 = {{1.0, 1.0, 1.0}, {2.0, 2.0, 2.0}, {3.0, 3.0, 3.0}};

            double[][] Expected_MatrizFinal = {{6.0, 6.0, 6.0}, {12.0, 12.0, 12.0}, {18.0, 18.0, 18.0}};

            System.out.println("Estado: " + test_multiplicarMatrizes(matriz1, matriz2, Expected_MatrizFinal));
        }

        teste = 5;
        if (teste == 5) {
            System.out.println();
            System.out.println("Teste 5- Posicao Igual");
            String string = "x010=30";
            System.out.println("Estado: " + test_posicaoIgual(string, 5));
        }


        teste = 6;
        if (teste == 6) {
            System.out.println();
            System.out.println("Teste 6- Distribuicao Final(Valor e Vetor próprio)");
            double[][] valorProprio={{1.63,0.00,0.00,0.00},
                                    {0.00,-0.65,0.00,0.00},
                                    {0.00,0.00,0.01,0.64},
                                    {0.00,0.00,-0.64,0.01}};

            double[][] vetorProprio={{-0.98,-1.25,1.87,-0.42},
                                    {-0.18,0.58,-0.19,-0.88},
                                    {-0.04,-0.36,-0.55,0.11},
                                    {-0.02,0.50,0.14,0.78}};

            double[] Expected_distribuicao={80.33,14.75,3.28,1.64};
            System.out.println("Estado: " + test_distruibuicaoFinal(vetorProprio, valorProprio, Expected_distribuicao));
        }

        teste = 7;
        if (teste == 7) {
            System.out.println();
            System.out.println("Teste 7- Maior Valor");
            double[][] matriz ={{1.63,0.00,0.00,0.00},
                    {0.00,-0.65,0.00,0.00},
                    {0.00,0.00,0.01,0.64},
                    {0.00,0.00,-0.64,0.01}};

            double Expected_MaiorValor=1.63;
            System.out.println("Estado: " + test_procurarMaiorValorProprio(matriz, Expected_MaiorValor));
        }

        teste = 8;
        if (teste == 8) {
            System.out.println();
            System.out.println("Teste 8- Maior coluna");
            double[][] matriz = {{1.63,0.00,0.00,0.00},
                                {0.00,-0.65,0.00,0.00},
                                {0.00,0.00,0.01,0.64},
                                {0.00,0.00,-0.64,0.01}};
            int Expected_colunaMaior=0;
            System.out.println("Estado: " + test_colunaMaiorNum(matriz, Expected_colunaMaior));
        }

    }


//                                        ---   MODULOS ---


//======================================================================================================================= DISTRIBUIÇÃO, TAXAS  E POP TOTAL


//Método: População total e distribuição etc.

    public static void distruibuicao_exe(double[][] MatrizLeslei, double[][] Pop, int geracoes, double[] Nt, double[] TaxasVariacao, double[][] ClassesTotal, double[][] ClassesNorm) {            //Aqui começa a executar todas as partes do tema
        int t = 0;
        double[][] calculo = Pop;
        double total1;
        total1 = calculoTotal(calculo);
        Nt[0] = total1;
        distribuicao(calculo, total1, t, ClassesTotal, ClassesNorm);
        if (geracoes > 0) {
            calculo = multiplicarMatrizes(MatrizLeslei, calculo);
            taxaVariacao(calculo, total1, t, TaxasVariacao);
        }
        if (geracoes >= 1) {
            calculo = multiplicarMatrizes(MatrizLeslei, Pop);
            for (int i = 2; i <= geracoes; i++) {                                              //repete o numero de gerações, começa no 2, pq k^1=k
                t++;
                total1 = calculoTotal(calculo);                                         //população total ( N(t) )
                Nt[t] = total1;
                distribuicao(calculo, total1, t, ClassesTotal, ClassesNorm);                                        //Distruibuição da população
                calculo = multiplicarMatrizes(MatrizLeslei, calculo);
                taxaVariacao(calculo, total1, t, TaxasVariacao);                                //atualizar resultado para t+1
            }
            total1 = calculoTotal(calculo);
            t++;
            Nt[t] = total1;
            distribuicao(calculo, total1, t, ClassesTotal, ClassesNorm);
        }

    }

    public static boolean test_taxaVariacao(double[][] matriz, double total, int t, double[] TaxasVariacao, double[] Expected_TaxasVariacao) {
        taxaVariacao(matriz, total, t, TaxasVariacao);
        return TaxasVariacao[t] == Expected_TaxasVariacao[0];
    }


//Método:Calculo das taxas de variação

    public static void taxaVariacao(double[][] calculo, double total, int t, double[] TaxasVariacao) {
        double total2 = calculoTotal(calculo);                                    //população total momento seguinte ------>     N(t+1)
        double taxa = total2 / total;
        double delta_t = round(taxa);
        TaxasVariacao[t] = delta_t;
    }


    public static boolean test_calculoTotal(double[][] matriz, double Expected_total) {
        double total = calculoTotal(matriz);
        return total == Expected_total;
    }
//Método:Calculo da população total

    public static double calculoTotal(double[][] calculo) {
        double total = 0;
        for (int i = 0; i < calculo.length; i++) {
            total += calculo[i][0];                 //Vai somando os valores do array das amostras
        }
        return total;
    }


    public static boolean test_distribuicao(double[][] calculo, double total, int t, double[][] ClassesTotal, double[][] ClassesNorm, double[][] Expected_DTotal, double[][] Expected_DNormal) {
        distribuicao(calculo, total, t, ClassesTotal, ClassesNorm);
        return (Arrays.deepEquals(ClassesNorm, Expected_DNormal) && (Arrays.deepEquals(ClassesTotal, Expected_DTotal)));
    }
//Método:Distribuição da população

    public static void distribuicao(double[][] calculo, double total, int t, double[][] ClassesTotal, double[][] ClassesNorm) {
        for (int i = 0; i < calculo.length; i++) {
            double resultado = ((calculo[i][0] / total) * 100);
            ClassesTotal[t][i] = calculo[i][0];
            ClassesNorm[t][i] = round(resultado);
        }
    }


    public static boolean test_multiplicarMatrizes(double[][] matriz1, double[][] matriz2, double[][] Expected_matrizFinal) {
        double[][] resultado = multiplicarMatrizes(matriz1, matriz2);
        return Arrays.deepEquals(resultado, Expected_matrizFinal);
    }

//Método: Multiplicação de Matrizes

    public static double[][] multiplicarMatrizes(double[][] arr1, double[][] arr2) {
        double[][] resultado = new double[arr1.length][arr2[0].length];
        for (int linha = 0; linha < arr1.length; linha++) {
            for (int coluna = 0; coluna < arr2[0].length; coluna++) {
                resultado[linha][coluna] = 0;
                for (int k = 0; k < arr2.length; k++) {
                    resultado[linha][coluna] += arr1[linha][k] * arr2[k][coluna];  //Multiplica uma linha pela coluna da outra
                }
            }
        }
        return resultado;
    }

//Método:Arredondar Valores

    public static double round(double value) {
        return Math.round(value * 100.00) / 100.00;
    }


//Método:Mostra a Matriz

    public static void mostrarMatriz(double[][] arr) {
        for (int linha = 0; linha < arr.length; linha++) {
            for (int coluna = 0; coluna < arr[0].length; coluna++) {
                System.out.print(arr[linha][coluna] + " ");        //Arredondamento duas casas decimais
            }
            System.out.println();
        }
    }


//Método:Apagar pasta+conteudos

    public static void deleteFile(File element) {
        if (element.isDirectory()) {
            for (File sub : Objects.requireNonNull(element.listFiles())) {
                deleteFile(sub);
            }
        }
        element.delete();
    }


//====================================================================================================================== LEITURA DE DADOS


//Método:Posição do igual (=)

    public static int posicaoIgual(String string) { // Descubro em que posição está o igual porque só preciso
        // do que está para a frente dele;
        int contador = 0;
        char[] array = string.toCharArray(); // Passo cada caracter da String para um Array

        for (int i = 0; i < array.length; i++) {

            String str = String.valueOf(array[i]);

            if (!str.equals("=")) contador++;
            else {
                contador++;
                return contador;
            }
        }

        return contador;
    }

    public static boolean test_posicaoIgual(String string, int expectedPosicao) {

        int posicao = posicaoIgual(string);

        return expectedPosicao == posicao;
    }


//Método:Leitura do Ficheiro

    public static boolean lerFicheiro(double[][] vetorFinal, double[] taxaSiFinal, double[] taxaFiFinal, String nomeFicheiro) throws FileNotFoundException {
        Scanner in = new Scanner(new File(nomeFicheiro));
        int[] posicao = new int[200];
        String line = in.nextLine(); // Primeiro seleciona-se a primeira linha
        while (line.isEmpty()) {    // Se não tiver nada, vai estar sempre a avançar
            line = in.nextLine();  // para a próxima até encontrar alguma
        }


        for (int k = 1; k <= 3; k++) { // Vou ler 3 dados

            String[] linhaAtual = line.split(",");

            if (linhaAtual[0].startsWith("x")) { // Se achar o x, sei que estou a ler os valores do Vetor;

                String[] itensVetor = line.split(", "); // Criar um array em que cada valor está separado por ", ";

                for (int i = 0; i < vetorFinal.length; i++) {
                    posicao[i] = Integer.parseInt(itensVetor[i].substring(1, posicaoIgual(itensVetor[i]) - 1));
                    itensVetor[i] = itensVetor[i].substring(posicaoIgual(itensVetor[i])); // Uso o substring para tirar tudo o que não sejam o resultado;
                    vetorFinal[i][0] = Integer.parseInt(itensVetor[i]); // Passo o valor de String para Int;
                    if (i > 0) {
                        if ((posicao[i] <= posicao[i - 1]) || (posicao[i] != posicao[i - 1] + 1)) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    } else {
                        if (posicao[i] != 0) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    }
                }
            }

            if (linhaAtual[0].startsWith("s")) { // Se achar o s, sei que estou a ler os valores da TaxaSi;

                String[] itensSi = line.split(", ");

                for (int i = 0; i < taxaSiFinal.length; i++) {
                    posicao[i] = Integer.parseInt(itensSi[i].substring(1, posicaoIgual(itensSi[i]) - 1));
                    itensSi[i] = itensSi[i].substring(posicaoIgual(itensSi[i]));
                    taxaSiFinal[i] = Double.parseDouble(itensSi[i]); // Passo o valor de String para Double;

                    if (i > 0) {
                        if ((posicao[i] <= posicao[i - 1]) || (posicao[i] != posicao[i - 1] + 1)) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    } else {
                        if (posicao[i] != 0) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    }

                    if (taxaSiFinal[i] <= 0 || taxaSiFinal[i] >= 1) {  // o Valor tem de estar entre 0 e 1 (exclusive);
                        System.out.println("Valor inválido");
                        return false;
                    }

                }
            }

            if (linhaAtual[0].startsWith("f")) {  // Se achar o f, sei que estou a ler os valores da TaxaFi;
                String[] itensFi = line.split(", ");

                for (int i = 0; i < taxaFiFinal.length; i++) {

                    posicao[i] = Integer.parseInt(itensFi[i].substring(1, posicaoIgual(itensFi[i]) - 1));
                    itensFi[i] = itensFi[i].substring(posicaoIgual(itensFi[i]));
                    taxaFiFinal[i] = Double.parseDouble(itensFi[i]);
                    if (i > 0) {
                        if ((posicao[i] <= posicao[i - 1]) || (posicao[i] != posicao[i - 1] + 1)) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    } else {
                        if (posicao[i] != 0) {
                            System.out.println("Os dados não foram colocados corretamente");
                            return false;
                        }
                    }

                    if (taxaFiFinal[i] < 0) { // O valor tem de ser maior que 0 (inclusive)
                        System.out.println("Valor Inválido");
                        return false;
                    }
                }
            }
            if (k != 3) {                               // Se eu estiver na terceira iteração
                line = in.nextLine();      // significa que não vai ter mais nada pra ler
                while (line.isEmpty()) {              // depois;
                    line = in.nextLine();            // Se eu estiver nas duas primeiras tenho que ler
                }                                   // a próxima linha.
            }

        }
        in.close();
        return true;
    }


//Método:Calcular Numero de valores

    public static int calcularNumValores(String nomeFicheiro) throws FileNotFoundException {  // Vejo na primeira linha com conteúdo
        Scanner in = new Scanner(new File(nomeFicheiro));                    // o tamanho do array de tudo
        String line = in.nextLine();                                     // que está separado por ", "
        while (line.isEmpty()) {
            line = in.nextLine();
        }
        String[] itens = line.split(", ");

        if (itens[0].startsWith("s")) return (itens.length + 1); // Se a string começar por s, sei que estou
        // a lidar com a taxaSi logo o tamanho é mais 1
        return itens.length;
    }

    public static boolean test_calcularNumValores(int expectedNum, String nomeFicheiro) throws FileNotFoundException {

        int num = calcularNumValores(nomeFicheiro);

        return num == expectedNum;
    }


//Método: Converter para Matriz de Leslei

    public static double[][] atribuirMatrizLeslie(double[] taxaSi, double[] taxaFi) {

        int tamanho = taxaFi.length;
        double[][] matrizLeslie = new double[tamanho][tamanho];


        for (int coluna = 0; coluna < tamanho; coluna++) {
            matrizLeslie[0][coluna] = taxaFi[coluna];
        }

        int coluna = 0;
        int linha = 1;

        while (linha < tamanho) {

            matrizLeslie[linha][coluna] = taxaSi[coluna];
            coluna++;
            linha++;
        }
        return matrizLeslie;
    }

    public static boolean test_atribuirMatrizLeslie(double[] taxaSi, double[] taxaFi, double[][] expectedMatrizLeslie) {

        double[][] matrizLeslie = atribuirMatrizLeslie(taxaSi, taxaFi);

        return expectedMatrizLeslie == matrizLeslie;
    }


//Método: Leitura dos valores no modo Interativo (sem ficheiro)

    public static boolean lerVetoresEnormes(double[][] vetorEnorme, double[] taxaSiEnorme, double[] taxaFiEnorme) {

        String resposta;
        int i = 0;
        tamanhoSemFicheiro = 0;


        System.out.println("Deseja começar a inserir valores ou o nome de um ficheiro? (1.Valor)(2.Ficheiro)");
        resposta = input.next();
        while (!resposta.equals("1") && !resposta.equals("2")) {

            System.out.println("Resposta inválida, insira '1' ou '2':");
            resposta = input.next();
            input.nextLine();
        }

        if (resposta.equals("2")) return false;

        System.out.println("Pode inserir um nome de um ficheiro depois de inserir um valor.");


        do {
            System.out.println("Insira o valor de x0" + i);
            vetorEnorme[i][0] = input.nextInt();
            while (vetorEnorme[i][0] < 0) {

                System.out.println("Número inválido,insira novamente:");
                vetorEnorme[i][0] = input.nextInt();
            }

            i++;
            tamanhoSemFicheiro++;

            System.out.println("Deseja continuar a ler valores? (1.Sim)(2.Nao)(3.Ficheiro)");
            resposta = input.next();
            input.nextLine();

            while (!resposta.equals("1") && !resposta.equals("2") && !resposta.equals("3")) {

                System.out.println("Resposta inválida, insira '1' ou '2' ou '3':");
                resposta = input.next();
                input.nextLine();
            }

            if (resposta.equals("3")) return false;

        } while (resposta.equals("1"));

        for (int j = 0; j < tamanhoSemFicheiro - 1; j++) {


            System.out.println("Insira o valor de s0" + j);
            taxaSiEnorme[j] = input.nextDouble();

            while (taxaSiEnorme[j] <= 0 || taxaSiEnorme[j] >= 1) {

                System.out.println("Número inválido,insira novamente:");
                taxaSiEnorme[j] = input.nextDouble();
            }

            System.out.println("Deseja continuar a ler valores? (1.Sim)(2.Ficheiro)");
            resposta = input.next();
            input.nextLine();

            while (!resposta.equals("1") && !resposta.equals("2")) {

                System.out.println("Resposta inválida, insira '1' ou '2':");
                resposta = input.next();
                input.nextLine();
            }

            if (resposta.equals("2")) return false;

        }

        for (int k = 0; k < tamanhoSemFicheiro; k++) {


            System.out.println("Insira o valor de f0" + k);
            taxaFiEnorme[k] = input.nextDouble();

            while (taxaFiEnorme[k] < 0) {

                System.out.println("Número inválido,insira novamente:");
                taxaFiEnorme[k] = input.nextDouble();
            }

            if (k != tamanhoSemFicheiro - 1) {

                System.out.println("Deseja continuar a ler valores? (1.Sim)(2.Ficheiro)");
                resposta = input.next();
                input.nextLine();

                while (!resposta.equals("1") && !resposta.equals("2")) {

                    System.out.println("Resposta inválida, insira '1' ou '2':");
                    resposta = input.next();
                    input.nextLine();
                }
            }

            if (resposta.equals("2")) return false;
        }


        return true;
    }


//Método: Converter para vetor

    public static double[][] passarVetorEnormeNormal(double[][] vetor) {

        double[][] vetorFinal = new double[tamanhoSemFicheiro][1];

        for (int i = 0; i < tamanhoSemFicheiro; i++) {

            vetorFinal[i][0] = vetor[i][0];
        }
        return vetorFinal;
    }

    //===================================================================================================================== VALOR E VETOR PRORIO


    //Método: Prever Evolução e Distribuição

    public static double[] preverEvolucaoEDestribuicao(double[][] matrizLeslie, double[] maiorNumProprio) {


        Matrix matrizMatrix = new Basic2DMatrix(matrizLeslie);             // Cria uma matriz.
        EigenDecompositor eigenD = new EigenDecompositor(matrizMatrix);

        Matrix[] matD = eigenD.decompose();    //Obtem valores e vetores próprios fazendo "Eigen Decomposition".


        double[][] matrizVetores = matD[0].toDenseMatrix().toArray();       // Converte objeto Matrix (duas matrizes)
        double[][] matrizValores = matD[1].toDenseMatrix().toArray();      // para array Java;

        for (int linha = 0; linha < matrizVetores.length; linha++) {                                               // Vou percorar as duas matrizes
            for (int coluna = 0; coluna < matrizVetores.length; coluna++) {                                     // e vou arredondar tudo a duas casas
                // decimais (nota: Não sei porque mas se
                matrizValores[linha][coluna] = Math.round(matrizValores[linha][coluna] * 100) / 100.00;     // dividir por 100 inteiro não resulta por
                matrizVetores[linha][coluna] = Math.round(matrizVetores[linha][coluna] * 100) / 100.00;    // isso é essencial ser 100.00).
                // System.out.println(matrizValores[linha][coluna]);
            }
        }

        maiorNumProprio[0] = procurarMaiorValorProprio(matrizValores);


        // preverEvolucaoEspecie(maiorNumProprio[0], printWriter5);
        // preverDistruibuicaoFinal(distruibuicao, printWriter5);
        return distribuicaoFinal(matrizVetores, matrizValores); // ====               //double[] distruibuicao = distribuicaoFinal(matrizVetores, matrizValores);

    }

 /*   public static void preverDistruibuicaoFinal(double[] vetorProprioNormalizado, PrintWriter printWriter5) {  // Isto serve só pra printar.

        printWriter5.print("vetor proprio associado=(");
        for (int i = 0; i < vetorProprioNormalizado.length; i++) {


            if (i != vetorProprioNormalizado.length - 1)
                printWriter5.print(Math.round(vetorProprioNormalizado[i]) + ",");
            else
                printWriter5.print(Math.round(vetorProprioNormalizado[i])); // Quando o i é igual ao tamanho - 1 não metemos "," porque é o último.
        }
        printWriter5.print(")");
        printWriter5.println();
    }*/


//Método: Distribuição Final

    public static double[] distribuicaoFinal(double[][] vetorProprio, double[][] valorProprio) { // Para calcular vetor próprio associado.

        int coluna = colunaMaiorNum(valorProprio), tamanho = vetorProprio.length;
        double soma = 0;
        double[] distribuicao = new double[tamanho];


        for (int linha = 0; linha < tamanho; linha++) {   // Primeiro vou calcular a soma da coluna do maior valor próprio,
            soma = vetorProprio[linha][coluna] + soma;   // preciso do valor soma para normalizar o vetor.
        }

        for (int linha = 0; linha < tamanho; linha++) {
            distribuicao[linha] = Math.round((vetorProprio[linha][coluna] * 100 / soma) * 100) / 100.00; // Coloco os valores da primeira coluna noutro array.
        }
        return distribuicao;
    }

    public static boolean test_distruibuicaoFinal(double[][] vetorProprio, double[][] valorProprio, double[] expectedDistribuicao) {
        double[] distribuição=distribuicaoFinal(vetorProprio, valorProprio);
        for (int i=0;i<distribuição.length;i++){
            System.out.println(distribuição[i]);
        }
        return  Arrays.equals(distribuição,expectedDistribuicao);
    }


    public static boolean test_procurarMaiorValorProprio(double[][] matriz, double expectedNum) {

        return procurarMaiorValorProprio(matriz) == expectedNum;

    }


    //Método: Procurar maior valor próprio
    public static double procurarMaiorValorProprio(double[][] matriz) { // Procuro na matriz o maior numero que será
        // o maior valor próprio
        double maiorNum = matriz[0][0];
        int tamanho = matriz.length;

        for (int linha = 0; linha < tamanho; linha++) {
            for (int coluna = 0; coluna < tamanho; coluna++) {
                if (matriz[linha][coluna] > maiorNum) maiorNum = matriz[linha][coluna];
            }
        }

        return maiorNum;
    }

    public static boolean test_colunaMaiorNum(double[][] matriz, int expectedNum) {

        return colunaMaiorNum(matriz) == expectedNum;
    }


//Método: coluna dp maior valor próprio

    public static int colunaMaiorNum(double[][] matriz) { // Procuro em que coluna está o maior numero

        double maiorNum;
        int maiorColuna, tamanho;

        maiorNum = matriz[0][0];
        maiorColuna = 0;
        tamanho = matriz.length;

        for (int linha = 0; linha < tamanho; linha++) {
            for (int coluna = 0; coluna < tamanho; coluna++) {

                if (matriz[linha][coluna] > maiorNum) {

                    maiorNum = matriz[linha][coluna];
                    maiorColuna = coluna;
                }
            }
        }

        return maiorColuna;
    }

   /* public static void preverEvolucaoEspecie(double valorProprio, PrintWriter printWriter5) {
        printWriter5.println();
        printWriter5.println("Maior valor próprio e vetor associado");
        printWriter5.printf("%s%.4f", "lambda=", valorProprio);
        printWriter5.println();
    }*/


//Método: Cópia manual de array

    public static double[] passarTaxaSiEnormeNormal(double[] taxa) {

        double[] taxaFinal = new double[tamanhoSemFicheiro - 1];

        for (int i = 0; i < tamanhoSemFicheiro - 1; i++) {

            taxaFinal[i] = taxa[i];
        }

        return taxaFinal;
    }


//Método: Cópia manual de array

    public static double[] passarTaxaFiEnormeNormal(double[] taxa) {

        double[] taxaFinal = new double[tamanhoSemFicheiro];

        for (int i = 0; i < tamanhoSemFicheiro; i++) {

            taxaFinal[i] = taxa[i];
        }

        return taxaFinal;
    }


//======================================================================================================================= GRÁFICOS


//Método: Gráfico da População Total

    public static void poptotal_graph(String modo, String dir, int formato, String nome) throws IOException, InterruptedException {

        //Cria ficheiro .gp

        File file = new File("./Intermediate_Files/Poptotal.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter = new PrintWriter(file);


        //Escreve o script no ficheiro

        printWriter.println("set terminal png");
        printWriter.println("set output \"Intermediate_Files/populacaototal.png\"");       //instruções para o gnuplot
        printWriter.println("set title \"População total ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"População/unidade\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n" +
                "plot 'Intermediate_Files/Populacao_total.txt' with lines title 'População Total'");
        printWriter.close();


        //Executa o ficheiro

        Runtime rt = Runtime.getRuntime();         //executar ficheiro .gp
        rt.exec("wgnuplot Intermediate_Files/Poptotal.gp");


        boolean b;
        String tipo1 = "";
        String tipo2 = "";
        if (modo.equals("Interativo")) {


            //Abre a imagem

            TimeUnit.MILLISECONDS.sleep(300);
            System.out.println("(Gráfico aberto)");
            File f = new File("Intermediate_Files/populacaototal.png");
            Desktop d = Desktop.getDesktop();
            d.open(f);


            //Guardar?

            System.out.println("Quer guardar este gráfico?(1.Sim)(2.Nao)");
            String resposta = input.next();
            while (!(resposta.equals("1") || resposta.equals("2"))) {
                System.out.println("Resposta invalida.(1.Sim/2.Nao)");
                resposta = input.next();
            }


            //Formato

            b = resposta.equals("1");
            if (b) {
                System.out.println("Que formato quer para gravar o seu gráfico?");
                System.out.println("1:png 2:txt 3:eps");
                formato = input.nextInt();
                while ((formato < 1) || (formato >= 4)) {
                    System.out.println("Os seus valores não são válidos, por favor insira números entre 1 e 3");
                    formato = input.nextInt();
                }
            }
        } else b = true;

        //é defenido o formato em que o utilizador quer gravar a imagem
        if (formato == 1) {
            tipo1 = "png";
            tipo2 = "png;";
        }
        if (formato == 2) {
            tipo1 = "txt";
            tipo2 = "dumb";
        }
        if (formato == 3) {
            tipo1 = "eps";
            tipo2 = "postscript eps";
        }


        //Cria novo ficheiro .gp com o formato desejado

        File file1 = new File("./Intermediate_Files/Poptotal.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter1 = new PrintWriter(file1);
        printWriter1.println("set terminal " + tipo2);
        printWriter1.println("set output \"" + dir + "/Graficos/" + nome + "_" + "Graph_PopulacaoTotal." + tipo1 + "\"");
        printWriter1.println("set title \"População total ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"População/unidade\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n" +
                "plot 'Intermediate_Files/Populacao_total.txt' with lines title 'População Total'");


        printWriter1.close();


        //Executa no caso de querer gravar

        if (b) {
            Runtime rt1 = Runtime.getRuntime();
            rt1.exec("wgnuplot ./Intermediate_Files\\Poptotal.gp");
        }                                  //a imagem é aberta no ecrã através do ficheiro .png que já tinha sido criado

    }


//Método: Gráfico da Variação da População

    public static void variacaopop_graph(String modo, String dir, int formato, String nome) throws IOException, InterruptedException {

        //Cria ficheiro .gp

        File file = new File("./Intermediate_Files/variacaopop.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter = new PrintWriter(file);


        //Escreve o script no ficheiro

        printWriter.println("set terminal png");
        printWriter.println("set output \"./Intermediate_Files/variacaopop.png");       //instruções para o gnuplot
        printWriter.println("set title \"Variação da população ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Variação\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n" +
                "plot 'Intermediate_Files/Variacao.txt' with lines title 'Variação'");
        printWriter.close();


        //Executa o ficheiro

        Runtime rt = Runtime.getRuntime();         //executar ficheiro .gp
        rt.exec("wgnuplot ./Intermediate_Files\\variacaopop.gp");

        boolean b;
        String tipo1 = "";
        String tipo2 = "";
        if (modo.equals("Interativo")) {
            //Abre a imagem

            TimeUnit.MILLISECONDS.sleep(300);
            System.out.println("(Gráfico aberto)");
            File f = new File("./Intermediate_Files/variacaopop.png");
            Desktop d = Desktop.getDesktop();
            d.open(f);


            //Guardar?

            System.out.println("Quer guardar este gráfico?(1.Sim)(2.Nao)");
            String resposta = input.next();
            while (!(resposta.equals("1") || resposta.equals("2"))) {
                System.out.println("Resposta invalida.(1.Sim/2.Nao)");
                resposta = input.next();
            }


            //Formato

            b = resposta.equals("1");
            if (b) {
                System.out.println("Que formato quer para gravar o seu gráfico?");
                System.out.println("1:png 2:txt 3:eps");
                formato = input.nextInt();
                while ((formato < 1) || (formato >= 4)) {
                    System.out.println("Os seus valores não são válidos, por favor insira números entre 1 e 3");
                    formato = input.nextInt();
                }
            }
        } else b = true;
        //é defenido o formato em que o utilizador quer gravar a imagem
        if (formato == 1) {
            tipo1 = "png";
            tipo2 = "png;";
        }
        if (formato == 2) {
            tipo1 = "txt";
            tipo2 = "dumb";
        }
        if (formato == 3) {
            tipo1 = "eps";
            tipo2 = "postscript eps enhanced color font 'Helvetica,10'";
        }


        //Cria novo ficheiro .gp com o formato desejado

        File file1 = new File("./Intermediate_Files/variacaopop.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter1 = new PrintWriter(file1);
        printWriter1.println("set terminal " + tipo2);
        printWriter1.println("set output \"" + dir + "/Graficos/" + nome + "_" + "Graph_TaxasDeVariacao." + tipo1 + "\"");
        printWriter1.println("set title \"Variação ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Variação\"\n" +


                "\n" +
                "set style data linespoints\n" +
                "\n" +
                "plot 'Intermediate_Files/Variacao.txt' with lines title 'Variação'");


        printWriter1.close();
        //Executa no caso de querer gravar
        if (b) {
            Runtime rt1 = Runtime.getRuntime();
            rt1.exec("wgnuplot ./Intermediate_Files\\variacaopop.gp");
        }                                  //a imagem é aberta no ecrã através do ficheiro .png que já tinha sido criado

    }


//Método: Gráfico das Classes Não Normalizadas

    public static void classes_graph(String modo, String dir, int formato, int numclasses, String nome) throws IOException, InterruptedException {

        //Cria ficheiro .gp
        File file = new File("./Intermediate_Files/Classes.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter = new PrintWriter(file);

        //Escreve o script no ficheiro
        printWriter.println("set terminal png");
        printWriter.println("set output \"./Intermediate_Files/Classes.png");       //instruções para o gnuplot
        printWriter.println("set title \"Classes ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Classes\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n");
        printWriter.print("plot ");
        for (int i = 2; i <= numclasses + 1; i++) {

            printWriter.print("'./Intermediate_Files/Classes.txt' using 1:" + i + " with lines title 'Classe " + (i - 1) + "',");

        }
        printWriter.close();


        //Executa o ficheiro

        Runtime rt = Runtime.getRuntime();         //executar ficheiro .gp
        rt.exec("wgnuplot ./Intermediate_Files\\Classes.gp");

        String resposta;
        String tipo1 = "";
        String tipo2 = "";
        boolean b;
        if (modo.equals("Interativo")) {
            //Abre a imagem
            TimeUnit.MILLISECONDS.sleep(300);               //Delay para executar o script e criar a imagem corretamente
            System.out.println("(Gráfico aberto)");
            File f = new File("./Intermediate_Files/Classes.png");
            Desktop d = Desktop.getDesktop();
            d.open(f);


            //Guardar?
            System.out.println("Quer guardar este gráfico?(1.Sim)(2.Nao)");
            resposta = input.next();
            while (!(resposta.equals("1") || resposta.equals("2"))) {
                System.out.println("Resposta invalida.(1.Sim/2.Nao)");
                resposta = input.next();
            }


            //Formato

            b = resposta.equals("1");
            if (b) {
                System.out.println("Que formato quer para gravar o seu gráfico?");
                System.out.println("1:png 2:txt 3:eps");
                formato = input.nextInt();
                while ((formato < 1) || (formato >= 4)) {
                    System.out.println("Os seus valores não são válidos, por favor insira números entre 1 e 3");
                    formato = input.nextInt();
                }
            }
        } else b = true;
        //é defenido o formato em que o utilizador quer gravar a imagem
        if (formato == 1) {
            tipo1 = "png";
            tipo2 = "png;";
        }
        if (formato == 2) {
            tipo1 = "txt";
            tipo2 = "dumb";
        }
        if (formato == 3) {
            tipo1 = "eps";
            tipo2 = "postscript eps enhanced color font 'Helvetica,10'";
        }


        //Cria novo ficheiro .gp com o formato desejado
        File file1 = new File("./Intermediate_Files/Classes.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter1 = new PrintWriter(file1);
        printWriter1.println("set terminal " + tipo2);
        printWriter1.println("set output \"" + dir + "/Graficos/" + nome + "_" + "Graph_ClassesTotais." + tipo1 + "\"");
        printWriter1.println("set title \"Classes ao longo do tempo\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Classes\"\n" +


                "\n" +
                "set style data linespoints\n" +
                "\n");
        printWriter1.print("plot ");
        for (int i = 2; i <= numclasses + 1; i++) {

            printWriter1.print("'./Intermediate_Files/Classes.txt' using 1:" + i + " with lines title 'Classe " + (i - 1) + "', ");
        }


        printWriter1.close();

        //Executa no caso de querer gravar
        if (b) {
            Runtime rt1 = Runtime.getRuntime();
            rt1.exec("wgnuplot ./Intermediate_Files\\Classes.gp");
        }                                  //a imagem é aberta no ecrã através do ficheiro .png que já tinha sido criado

    }


//Método: Gráfico das Classes Normalizadas

    public static void normalizados_graph(String modo, String dir, int formato, int numclasses, String nome) throws IOException, InterruptedException {

        //Criar ficheiro .gp

        File file = new File("./Intermediate_Files/Normalizados.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter = new PrintWriter(file);


        //Script gnuplot

        printWriter.println("set terminal png");
        printWriter.println("set output \"./Intermediate_Files/Normalizados.png");       //instruções para o gnuplot
        printWriter.println("set title \"Classes ao longo do tempo em percentagem\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Classes em percentagem\"\n" +
                "\n" +
                "set style data linespoints\n" +
                "\n");
        printWriter.print("plot ");
        for (int i = 2; i <= numclasses + 1; i++) {
            printWriter.print("'./Intermediate_Files/Normalizados.txt' using 1:" + i + " with lines title 'Classe " + (i - 1) + "',");
        }
        printWriter.close();


        //Executar Script gnuplot

        Runtime rt = Runtime.getRuntime();         //executar ficheiro .gp
        rt.exec("wgnuplot ./Intermediate_Files\\Normalizados.gp");


        String tipo1 = "";
        String tipo2 = "";
        boolean b;
        if (modo.equals("Interativo")) {
            //Abrir imagem .png inicial

            TimeUnit.MILLISECONDS.sleep(300);   //Para dar tempo para processar o script e criar imagem
            File f = new File("./Intermediate_Files/Normalizados.png");
            Desktop d = Desktop.getDesktop();
            d.open(f);
            System.out.println("(Gráfico aberto)");

            //Quer gravar ?
            System.out.println("Quer guardar este gráfico?(1.Sim)(2.Nao)");
            String resposta = input.next();
            while (!(resposta.equals("1") || resposta.equals("2"))) {
                System.out.println("Resposta invalida.(1.Sim/2.Nao)");
                resposta = input.next();
            }
            b = resposta.equals("1");
            if (b) {
                //formato da imagem
                System.out.println("Que formato quer para gravar o seu gráfico?");
                System.out.println("1:png 2:txt 3:eps");
                formato = input.nextInt();
                while ((formato < 1) || (formato > 3)) {
                    System.out.println("Os seus valores não são válidos, por favor insira números entre 1 e 3");
                    formato = input.nextInt();
                }
            }
        } else b = true;
        //é defenido o formato em que o utilizador quer gravar a imagem
        if (formato == 1) {
            tipo1 = "png";
            tipo2 = "png;";
        }
        if (formato == 2) {
            tipo1 = "txt";
            tipo2 = "dumb";
        }
        if (formato == 3) {
            tipo1 = "eps";
            tipo2 = "postscript eps enhanced color font 'Helvetica,10'";
        }


        //Criar novo script para o formato desejado

        File file1 = new File("./Intermediate_Files/Normalizados.gp");   //Cria ficheiro dentro da pasta
        PrintWriter printWriter1 = new PrintWriter(file1);
        printWriter1.println("set terminal " + tipo2);
        printWriter1.println("set output \"" + dir + "/Graficos/" + nome + "_" + "Graph_ClassesNormalizadas." + tipo1 + "\"");
        printWriter1.println("set title \"Classes ao longo do tempo em percentagem\"\n" +
                "set xlabel \"Tempo/t\"\n" +
                "set ylabel \"Classes em percentagem\"\n" +


                "\n" +
                "set style data linespoints\n" +
                "\n");
        printWriter1.print("plot ");
        for (int i = 2; i <= numclasses + 1; i++) {

            printWriter1.print("'./Intermediate_Files/Normalizados.txt' using 1:" + i + " with lines title 'Classe " + (i - 1) + "', ");
        }


        printWriter1.close();

        //Executar o script criado
        if (b) {
            Runtime rt1 = Runtime.getRuntime();
            rt1.exec("wgnuplot ./Intermediate_Files\\Normalizados.gp");
        }                                  //a imagem é aberta no ecrã através do ficheiro .png que já tinha sido criado
    }
}